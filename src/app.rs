use core::fmt::Debug;
use rust_gl::attribute::{Attribute, Pointer};
use rust_gl::binder::allocate::Allocate;
use rust_gl::binder::TargetBind;
use rust_gl::context::Context;
use rust_gl::derive::{ToAttribute, ToUniform};
use rust_gl::math::*;
use rust_gl::vector;
use std::error::Error;
use web_sys::WebGl2RenderingContext;

#[derive(ToAttribute)]
struct Attributes<T> {
    vertex: Vector3<T>,
    normal: Vector3<T>,
}

#[derive(ToUniform)]
struct Uniforms {
    projection : Matrix4<f32>,
    view : Matrix4<f32>, 
    model : Matrix4<f32>,
    light_pos : Vector3<f32>,
    light_color : Vector3<f32>, 
    object_color : Vector3<f32>,
    ambient_strength : f32, 
    specular_strength : f32, 
    shine_strength : f32,
}

pub async fn app(mut context: Context) -> Result<!, Box<dyn Error>> {
    use rust_gl::program::ShaderCreator;
    let vertex = context.shader(include_str! {"shader/vertex.glsl"})?;
    let fragment = context.shader(include_str! {"shader/fragment.glsl"})?;

    let mut program = context
        .program(&vertex, &fragment)
        .attributes(Attributes {
            vertex: vector! {0.,0.,0.},
            normal: vector! {0.,0.,0.},
        })
        .uniforms(Uniforms {
            projection : Matrix::perspective(0.785398163, 1., 1., 4.).map(|x| x as f32),
            view : Matrix::translate([0., 0., -2., 1.]),
            model : Matrix::scale([0.2,0.2,0.2,1.]),
            light_pos : vector! {2.,2.,2.},
            light_color : vector! {1.,1.,1.},
            object_color : vector! {1.,0.,0.},
            ambient_strength : 0.1,
            specular_strength : 0.5,
            shine_strength : 32.0,
        })
        .build()?;

    let mut using = context.using(&mut program);

    using.enable(WebGl2RenderingContext::DEPTH_TEST);

    using.clear_color(0.3921, 0.5843, 0.9294, 1.0);

    let mut sphere = using
        .context
        .load(include_bytes! {"obj/sphere.obj"}.as_ref())?;

    let mut vert_ptr = using.program.attributes.vertex.pointer(sphere.ptr.vertex);

    let mut norm_ptr = using.program.attributes.normal.pointer(sphere.ptr.normal);

    loop {
        draw! {
            using.context.clear(
                WebGl2RenderingContext::COLOR_BUFFER_BIT | WebGl2RenderingContext::DEPTH_BUFFER_BIT 
            );

            let mut binder = using.context.array_buffer.bind(&mut sphere.vertices);

            let _vert = binder.pointer(&mut vert_ptr);

            let _norm = binder.pointer(&mut norm_ptr);

            using.context.draw_elements::<u16>(WebGl2RenderingContext::TRIANGLES, 0..sphere.size);
        }
    }
}
