#version 300 es
in vec3 vertex, normal;

out vec3 frag_pos, frag_normal, frag_light_pos;

uniform vec3 light_pos;

uniform mat4 model, view, projection;

void main()
{
    gl_Position = projection * view * model * vec4(vertex, 1.0);
    frag_pos = vec3(view * model * vec4(vertex, 1.0));
    frag_normal = mat3(transpose(inverse(view * model))) * normal;
    frag_light_pos = vec3(view * vec4(light_pos, 1.0)); 
}
