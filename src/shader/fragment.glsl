#version 300 es
precision highp float;

out vec4 frag_color;

in vec3 frag_pos, frag_normal, frag_light_pos;  

uniform vec3 light_color, object_color;

uniform float ambient_strength, specular_strength, shine_strength;

void main()
{
    // ambient
    
    vec3 ambient = ambient_strength * light_color;    
    
     // diffuse 
    vec3 norm = normalize(frag_normal);
    vec3 light_dir = normalize(frag_light_pos - frag_pos);
    float diff = max(dot(norm, light_dir), 0.0);
    vec3 diffuse = diff * light_color;
    
    // specular
    vec3 view_dir = normalize(-frag_pos); 
    vec3 reflect_dir = reflect(-light_dir, norm);  
    float spec = pow(max(dot(view_dir, reflect_dir), 0.0), shine_strength);
    vec3 specular = specular_strength * spec * light_color; 
    
    vec3 result = (ambient + diffuse + specular) * object_color;
    frag_color = vec4(result, 1.0);
}