#![feature(never_type)]

use wasm_bindgen::prelude::wasm_bindgen;

#[wasm_bindgen]
extern "C" {
    fn alert(s: &str);
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);
}

//Macro wapper for the js alert function.
macro_rules! alert {
    ($($t:tt)*) => ($crate::alert(&format_args!($($t)*).to_string()))
}

//Macro wapper for the js console.log function.
macro_rules! log {
    ($($t:tt)*) => ($crate::log(&format_args!($($t)*).to_string()))
}

//Asyncrous scope, yields at the start of the scope.
macro_rules! draw{
    ($($t:tt)*) => {
        {
            use futures_lite::future::yield_now;
            yield_now().await;
            $($t)*
        }
    }

}

pub mod js;
//This is the function that wasm is searching for.
#[wasm_bindgen]
pub fn start() {
    //Spawn an async function and run it.
    js::run(main());
}

pub mod app;
pub mod attributes;
use attributes::*;
use std::error::Error;
use wasm_bindgen::JsValue;

pub async fn main() -> Result<(), Box<dyn Error>> {
    use wasm_bindgen::JsCast;
    use web_sys::{window, HtmlCanvasElement, WebGl2RenderingContext};

    //Retrive the html document from the browser.
    let document = window()
        .ok_or("Unable to get window")?
        .document()
        .ok_or("Unable to get document")?;

    //Retrive the html canvas from the document.
    let canvas = document
        .get_element_by_id("canvas")
        .ok_or("Unable to get canvas")?
        .dyn_into::<HtmlCanvasElement>()
        .map_err(|_| "Unable to cast to Canvas")?;

    let attributes = Attributes {
        depth: true,
        ..Default::default()
    };
    let js_value = JsValue::from_serde(&attributes)?;

    use rust_gl::context::Context;
    //Retrive the context from the canvas.
    let context: Context = canvas
        .get_context_with_context_options("webgl2", &js_value)
        .ok()
        .flatten()
        .ok_or("Unable to get webgl2 context")?
        .dyn_into::<WebGl2RenderingContext>()
        .map_err(|_| "Unable to cast to webgl2 context")?
        .into();

    //Run the app function defined in app.rs
    app::app(context).await?;
}
