use wasm_bindgen::closure::Closure;
///Convert the given rust closure, to web assembly closure.
pub fn js_function<F>(function: F) -> Closure<dyn FnMut()>
where
    F: 'static + FnMut(),
{
    Closure::wrap(Box::new(function) as Box<dyn FnMut()>)
}

///Request the browser to run the web assembly closure as an animation frame.
pub fn request_animation_frame(f: &Closure<dyn FnMut()>) {
    use wasm_bindgen::JsCast;
    if let Err(x) = web_sys::window()
        .unwrap()
        .request_animation_frame(f.as_ref().unchecked_ref())
    {
        alert! {"{:?}", x};
    }
}

use core::future::Future;
use std::cell::RefCell;
use std::error::Error;
use std::rc::Rc;
///Run the given future to completion, if the future returns an error, alert the browser.
pub fn run<F>(future: F)
where
    F: 'static + Future<Output = Result<(), Box<dyn Error>>>,
{
    use futures::task::{noop_waker_ref, Context};
    //The given can never inform the browser that it is ready, as such a dummy context is used.
    let mut context = Context::from_waker(noop_waker_ref());

    let original = Rc::new(RefCell::new(None));
    //The closure will take ownership of this refernce counter.
    let reference = original.clone();

    //Pin the future in heap space, this will allow for futures that does not impliment the unpin trait.
    let mut pin = Box::pin(future);

    *original.borrow_mut() = Some(js_function(move || {
        use core::task::Poll::*;
        use futures_lite::future::FutureExt;
        match pin.poll(&mut context) {
            //If the future is finished, do nothing.
            Ready(Ok(())) => (),
            //If the future returned an error, alert the browser.
            Ready(Err(x)) => alert! {"{}", x},
            //If the future is not finished, request a new animation frame from the browser.
            Pending => {
                request_animation_frame(reference.borrow().as_ref().unwrap());
            }
        }
    }));
    request_animation_frame(original.borrow().as_ref().unwrap());
}
