use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "kebab-case")]
pub enum PowerPreference{
    Default,
    HighPerformance,
    LowPower,
}

impl Default for PowerPreference{
    fn default() -> Self{
        Self::Default
    }
}

#[derive(Serialize, Deserialize, Debug, Default)]
#[serde(rename_all = "camelCase")]
pub struct Attributes{
    pub alpha : bool,
    pub desynchronized : bool,
    pub antialias : bool,
    pub depth : bool,
    pub power_preference : PowerPreference,
    pub fail_if_major_performance_caveat : bool,
    pub premultiplied_alpha : bool,
    pub preserve_drawing_buffer : bool,
    pub stencil: bool,
}
