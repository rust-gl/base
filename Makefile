build:
	wasm-pack build

update: 
	sudo npm install --prefix www

run:
	npm run start --prefix www

see:
	sensible-browser http://localhost:8080/ &

test:
	wasm-pack test --headless --firefox

install: 
	sudo apt install -y -f pkg-config libssl1.0-dev nodejs nodejs-dev node-gyp npm
	sudo npm cache clean -f
	sudo npm install -g n 
	sudo n stable
	sudo npm install --prefix www
	cargo install wasm-pack
	
WASM=$(shell wasm-pack -v dot 2> /dev/null)
ifndef WASM
	curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh
endif
	
